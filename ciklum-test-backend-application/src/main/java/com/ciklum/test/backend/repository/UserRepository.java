package com.ciklum.test.backend.repository;

import com.ciklum.test.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findUserById(Long userId);
    User findUserByLogin(String login);
}

package com.ciklum.test.backend.service.api;

import com.ciklum.test.backend.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    User saveUser(User user);
    User getUser(Long userId);
    User getUser(String login);
}

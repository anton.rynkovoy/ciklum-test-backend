package com.ciklum.test.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiklumTestBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CiklumTestBackendApplication.class, args);
	}

}


package com.ciklum.test.backend.service;

import com.ciklum.test.backend.model.User;
import com.ciklum.test.backend.repository.UserRepository;
import com.ciklum.test.backend.service.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User getUser(Long userId) {
        return userRepository.findUserById(userId);
    }

    @Override
    public User getUser(String login) {
        return userRepository.findUserByLogin(login);
    }
}
